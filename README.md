# FireGo

**Author** - Radek Kučera

## Description

Easy firestore setup with Go language.

## Usage

- Put your firebase-admin-sdk **serviceAccountKey.json** into **./configs** folder.

- You can start using your firestore in **./cmd/firego/main.go** or import it anywhere.
