package main

import (
	"context"
	"fire-go/db"
	"log"
)

var path string = "../../configs/serviceAccountKey.json"

var data map[string]string = map[string]string{"hello": "world"}

func main() {
	client, err := db.InitFirestore(path)
	if err == nil {

		res, err := client.Collection("test").Doc("test").Set(context.Background(), data)
		if err == nil {
			log.Println(res)
		}

	}
	defer client.Close()
}
