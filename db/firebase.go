package db

import (
	"log"
	"context"
	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
	"cloud.google.com/go/firestore"
)

// InitFirestore is function to initialize Firestore database
func InitFirestore(keyPath string) (*firestore.Client, error){
	sa := option.WithCredentialsFile(keyPath)
	
	app, err := firebase.NewApp(context.Background(), nil, sa)
	if err != nil {
		log.Fatalln(err)
	}
	
	client, err := app.Firestore(context.Background())

	return client, err
}